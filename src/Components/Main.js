import React, { Component } from 'react'
import Experience from './Experience/Experience';
import Education from './Education/Education'
import img from './img/tony.jpg'
import Dashboard from './Dashboard/Dashboard';
import Skills from './Experience/Skills/Skills';
import Language from './Langauge/Language';
import { Route, Link } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import EducationModal from './Education/educationModal';
class Profile extends Component {

    render() {
        let info = (
            <div className="w3-display-container">
                <img src={img} style={{ width: '100%' }} alt="Avatar" />
                <div className="w3-display-bottomleft w3-container w3-text-black">
                    <h2>Jane Doe</h2>
                </div>
            </div>
        );
        let personal = (
            <div>
                <p><i className="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Designer</p>
                <p><i className="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>London, UK</p>
                <p><i className="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>ex@mail.com</p>
                <p><i className="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>1224435534</p>
            </div>
        )
        return (
            <BrowserRouter>
                <div className="w3-content w3-margin-top" style={{ maxWidth: '1000px' }}>
                    <div className="w3-row-padding">
                    </div>
                    <div className="w3-row-padding">
                        <div className="w3-third">

                            <div className="w3-white w3-text-grey w3-card-4">
                                {info}
                                <div className="w3-container">
                                    {personal}
                                    <hr />
                                    <br />
                                    <header>
                                        <nav>
                                            <Link to={"/"}><p><i className="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>Home</p></Link>
                                        </nav>
                                        <nav>
                                            <Link to={"/education"}><p><i className="fa fa-certificate fa-fw w3-margin-right w3-large w3-text-teal"></i>Education</p></Link>
                                        </nav>
                                        <nav>
                                            <Link to="/skill"><p><i className="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>Skills</p></Link>
                                        </nav>

                                        <nav>
                                            <Link to="/lang"><p><i className="fa fa-globe fa-fw w3-margin-right w3-large w3-text-teal"></i>Language</p></Link>
                                        </nav>

                                        <nav>
                                            <Link to="/upload"><p><i className="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>Upload Resume</p></Link>
                                        </nav>
                                    </header>
                                    <br />
                                </div>
                            </div><br />


                        </div>
                        <div className="w3-twothird">
                            <Route path="/" exact component={Experience}></Route>
                            <Route path="/education" exact component={()=>{
                                return <Education id={10}/>
                            }}></Route>
                            <Route path="/skill" exact component={Skills}></Route>
                            <Route path="/lang" exact component={Language}></Route>
                        </div>

                    </div>
                </div>
            </BrowserRouter>
        )
    }
}


export default Profile;




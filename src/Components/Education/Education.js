import React, { Component } from 'react'

import { css } from '@emotion/core';
// First way to import
import { ClipLoader, BounceLoader, DotLoader, RingLoader, PulseLoader, RotateLoader, ScaleLoader } from 'react-spinners';
// Another way to import
import PropTypes from 'prop-types';
import '../css/common.css'
import Axios from 'axios'

class Education extends Component {

    state = {
        educationdata: []
    }
    componentDidMount() {
        debugger;
        Axios.get('http://localhost:8080/api/education').then(res => {
            debugger;
            console.log(res.data.data)
            this.setState({
                educationdata: res.data.data
            });
            // 



        }).catch({

        })
    }


    render() {
        debugger;


        let loader = <div className='sweet-loading'>
            <ScaleLoader
                sizeUnit={"px"}
                size={200}
                color={'red'}
                radius={30}
            />
        </div>


        loader = this.state.educationdata.map(data => {
            if (this.state.educationdata.length > 0) {
                return (
                    <div className="w3-container">
                    
                        <h5 className="w3-opacity"><b>{data.schoolName}</b></h5>
                        <i className="fa fa-pencil-square-o w3-margin-right"></i>
                        <h6 className="w3-text-teal"><i className="fa fa-calendar fa-fw w3-margin-right"></i>Forever</h6>
                        <p>{data.degree}</p>
                        <hr />
                    </div>



                )
            }
        })


        return (
            <div className="w3-container w3-card w3-white " >
                <h2 className="w3-text-grey w3-padding-16"><i className="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Education</h2>

                {loader}
            </div>

        )
    }
}
ClipLoader.propTypes = {
    loading: true,
    color: '#000000',
    css: {},



};
export default Education;
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Profile from '../src/Components/Main';

import Axios from 'axios'

// Axios.defaults.baseURL="http://localhost:8080/api";
// Axios.defaults.headers.common['Authorization']="AUTH TOKEN";
// Axios.defaults.headers.post["Content-type"]="application/json";

ReactDOM.render(<Profile />, document.getElementById('root'));



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
